package lab.movi.web.servlet;

import lab.movi.model.Cinema;
import lab.movi.model.Movie;
import lab.movi.service.MoviService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.function.Supplier;
import java.util.logging.Logger;

//@WebServlet("/cinemas")
public class CinemasServlet extends HttpServlet {

    Logger logger = Logger.getLogger(CinemasServlet.class.getName());

    @Inject
    private MoviService moviService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("fetching cinemas list");
        logger.fine("trace cinemas list");

        String movieIdString = req.getParameter("movieId");

        String title = "Cinemas";
        Supplier<Set<Cinema>> supplier = () -> moviService.getAllCinemas();
        if(movieIdString!=null){
            logger.info("fetching cinemas list playing movie " + movieIdString);
            Movie movie = moviService.getMovieById(Integer.parseInt(movieIdString));
            title = title + " playing " + movie.getTitle();
            supplier = () -> moviService.getCinemasByMovie(movie);
        } else {
            logger.info("fetching cinemas list");
        }
        req.setAttribute("title", title);
        req.setAttribute("cinemas", supplier.get());

        req.getRequestDispatcher("/WEB-INF/jsp/cinemas.jsp").forward(req, resp);
    }
}
