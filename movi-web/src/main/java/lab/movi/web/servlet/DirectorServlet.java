package lab.movi.web.servlet;

import lab.movi.service.MoviService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

public class DirectorServlet extends HttpServlet {

    Logger logger = Logger.getLogger(DirectorServlet.class.getName());

    @Inject
    private MoviService moviService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("about to fetch directors list");

        req.setAttribute("title", "Directors list");
        req.setAttribute("directors", moviService.getAllDirectors());

        req.getRequestDispatcher("/WEB-INF/jsp/directors.jsp").forward(req, resp);
    }
}
