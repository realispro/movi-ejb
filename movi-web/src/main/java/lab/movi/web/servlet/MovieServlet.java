package lab.movi.web.servlet;

import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;
import lab.movi.service.MoviService;


import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class MovieServlet extends HttpServlet {

    Logger logger = Logger.getLogger(MovieServlet.class.getName());

    @Inject
    private MoviService moviService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String directorIdString = req.getParameter("directorId");
        String cinemaIdString = req.getParameter("cinemaId");

        String title = "Movies";

        Supplier<Set<Movie>> supplier;

        if(directorIdString!=null){
            logger.info("fetching movies of director " + directorIdString);
            int directorId = Integer.parseInt(directorIdString);
            Director d = moviService.getDirectorById(directorId);
            title = title +  " of director " + d.getFirstName() + " " + d.getLastName();
            supplier = () ->  moviService.getMoviesByDirector(d);
        } else if(cinemaIdString!=null){
            logger.info("fetching movies played in cinema " + cinemaIdString);
            int cinemaId = Integer.parseInt(cinemaIdString);
            Cinema c = moviService.getCinemaById(cinemaId);
            title = title +  " played in  " + c.getName();
            supplier = () -> moviService.getMoviesInCinema(c);
            req.setAttribute("ticket", true);
            req.setAttribute("cinemaId", cinemaId);
        } else {
            logger.info("fetching movies");
            supplier = () -> moviService.getAllMovies();
        }

        req.setAttribute("movies",supplier.get());
        req.setAttribute("title", title);

        req.getRequestDispatcher("/WEB-INF/jsp/movies.jsp").forward(req, resp);
    }
}
