package lab.movi.web.servlet;

import lab.movi.model.Cinema;
import lab.movi.model.Movie;
import lab.movi.model.Ticket;
import lab.movi.service.MoviService;
import lab.movi.service.TicketService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/ticket")
public class TicketServlet extends HttpServlet {


    @Inject
    TicketService ticketService;

    @Inject
    MoviService moviService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cinema c = moviService.getCinemaById(
                Integer.parseInt(req.getParameter("cinemaId"))
        );

        Movie m = moviService.getMovieById(
                Integer.parseInt(req.getParameter("movieId"))
        );

        Ticket t = ticketService.buyTicket(c, m);

        resp.sendRedirect(req.getContextPath() + "/movies");

    }
}
