<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>


<jsp:include page="./header.jsp" flush="true"/>

            <table>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Show movies</th>
                </tr>
                <c:forEach var="c" items="${cinemas}">
                    <tr>
                        <td>
                            <img src="${c.logo}"/>
                        </td>
                        <td>
                                ${c.name}
                        </td>
                        <td>
                            <a href="./movies?cinemaId=${c.id}">movies</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>

            <jsp:include page="./footer.jsp" flush="true"/>
