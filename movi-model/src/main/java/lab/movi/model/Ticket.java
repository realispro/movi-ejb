package lab.movi.model;

import java.io.Serializable;

public class Ticket implements Serializable {

    private Cinema cinema;

    private Movie movie;

    public Ticket(Cinema cinema, Movie movie) {
        this.cinema = cinema;
        this.movie = movie;
    }

    public Ticket() {
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "cinema=" + cinema +
                ", movie=" + movie +
                '}';
    }
}
