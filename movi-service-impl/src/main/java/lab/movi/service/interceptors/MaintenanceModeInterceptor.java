package lab.movi.service.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.logging.Logger;

public class MaintenanceModeInterceptor {

    Logger logger = Logger.getLogger(MaintenanceModeInterceptor.class.getName());

    private int opening = 9;

    private int closing = 17;

    @AroundInvoke
    public Object intercept(InvocationContext ctx) throws Exception {

        logger.info("intercepting method " +
                ctx.getMethod().getName() +
                " on " + ctx.getTarget().toString() +
                " with args " + Arrays.toString(ctx.getParameters()));


        int now = LocalTime.now().getHour();
        if(now>=closing || now<opening){
            throw new IllegalAccessException("the system is in maintenance mode currently");
        }


        Object o = ctx.proceed();

        return o;


    }

}
