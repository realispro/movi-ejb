package lab.movi.service.impl;

import lab.movi.model.Cinema;
import lab.movi.model.Movie;
import lab.movi.model.Ticket;
import lab.movi.service.TicketService;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jms.*;
import java.util.logging.Logger;

@Stateless
@Local(TicketService.class)
public class TicketServiceBean implements TicketService {

    Logger logger = Logger.getLogger(TicketServiceBean.class.getName());

    //@Resource(lookup = "java:/ConnectionFactory")
    private ConnectionFactory cf;

    //@Resource(lookup = "java:/jms/queue/TicketQueue")
    private Queue queue;


    @Override
    public Ticket buyTicket(Cinema c, Movie m) {
        logger.info("about to buy ticket to " + c + " on " + m);

        Ticket t = new Ticket(c, m);

        try(Connection conn = cf.createConnection();){

            Session session = conn.createSession();
            MessageProducer mp = session.createProducer(queue);
            Message message = session.createObjectMessage(t);

            mp.send(message);


        } catch (JMSException e) {
            e.printStackTrace();
            throw new RuntimeException("ticket queueing failed", e);
        }

        return t;
    }
}
