package lab.movi.service.impl;

import lab.movi.dao.CinemaDao;
import lab.movi.dao.DirectorDao;
import lab.movi.dao.MovieDao;
import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;
import lab.movi.service.MoviService;
import lab.movi.service.MoviServiceRemote;
import lab.movi.service.TicketService;
import lab.movi.service.interceptors.MaintenanceModeInterceptor;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.jws.WebService;
import javax.transaction.*;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

@Stateless(name="MoviService")
@Local(MoviService.class)
//@Remote(MoviServiceRemote.class)
@WebService
//@Pool("my-pool")
//@Interceptors(MaintenanceModeInterceptor.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class MoviServiceImpl implements MoviService {

    Logger logger = Logger.getLogger(MoviService.class.getName());

    private static AtomicInteger count = new AtomicInteger(0);

    @Inject
    private DirectorDao directorDao;

    @Inject
    private CinemaDao cinemaDao;

    @Inject
    private MovieDao movieDao;

    @Resource
    private UserTransaction transaction;


    //@Interceptors(MaintenanceModeInterceptor.class)
    public Set<Movie> getAllMovies() {
        logger.info("searching all movies...");
        return movieDao.findAll();
    }

    public Set<Movie> getMoviesByDirector(Director d) {
        logger.info("serching movies by diretor " + d.getId());
        return movieDao.findByDirector(d);
    }

    public Set<Movie> getMoviesInCinema(Cinema c) {
        logger.info("searching movies played in cinema " + c.getId());
        return movieDao.findByCinema(c);
    }

    public Movie getMovieById(int id) {
        logger.info("searching movie by id " + id);
        return movieDao.findById(id);
    }

    public Set<Cinema> getAllCinemas() {
        logger.info("searching all cinemas");
        return cinemaDao.findAll();
    }

    public Set<Cinema> getCinemasByMovie(Movie m) {
        logger.info("searching cinemas by movie " + m.getId());
        return cinemaDao.findByMovie(m);
    }

    public Cinema getCinemaById(int id) {
        logger.info("searching cinema by id " + id);
        return cinemaDao.findById(id);
    }

    public Set<Director> getAllDirectors() {
        logger.info("searching all directors");
        return directorDao.findAll();
    }

    public Director getDirectorById(int id) {
        logger.info("searching director by id " + id);
        return directorDao.findById(id);
    }

    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Movie addMovie(Movie m) {
        logger.info("about to add movie " + m );
        try {
            transaction.begin();
            m = movieDao.add(m);
            transaction.commit();
            return m;
        }  catch (Exception e) {
            e.printStackTrace();
            try {
                transaction.rollback();
            } catch (SystemException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException("problem while adding movie", e);
        }
    }

    @Override
    //@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Director addDirector(Director d) {
        logger.info("about to add director " + d);

        try {
            transaction.begin();
            d = directorDao.add(d);
            transaction.commit();
            return d;
        }  catch (Exception e) {
            e.printStackTrace();
            try {
                transaction.rollback();
            } catch (SystemException ex) {
                ex.printStackTrace();
            }
            throw new RuntimeException("problem while adding director", e);
        }

    }

    @PostConstruct
    public void postConstruct(){

        logger.info("***** creating instance no " + count.incrementAndGet());
    }

    @PreDestroy
    public void preDestory(){
        logger.info("***** destroying instance no " + count.getAndDecrement());
    }

}
