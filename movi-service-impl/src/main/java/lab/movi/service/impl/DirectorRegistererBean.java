package lab.movi.service.impl;

import lab.movi.model.Director;
import lab.movi.model.Movie;
import lab.movi.service.DirectorRegisterer;
import lab.movi.service.MoviService;
import lab.movi.service.interceptors.MaintenanceModeInterceptor;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.Set;
import java.util.logging.Logger;

/*@Stateful
@Remote(DirectorRegisterer.class)
@Interceptors(MaintenanceModeInterceptor.class)
@TransactionManagement(TransactionManagementType.CONTAINER)*/
public class DirectorRegistererBean implements DirectorRegisterer {

    Logger logger = Logger.getLogger(DirectorRegistererBean.class.getName());

    @Inject
    private MoviService moviService;

    private Director director;

    @Override
    public void createDirector(Director director) {
        logger.info("creating director " + director);
        this.director = director;
    }

    @Override
    public void addMovie(Movie movie) {
        if(director==null){
            throw new IllegalStateException("add director first");
        }
        logger.info("adding movie " + movie);
        this.director.addMovie(movie);

    }


    @Override
    @Remove
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean register() {

        Set<Movie> movies = director.getMovies();
        director = moviService.addDirector(director); // NOT_SUPPORTED

        int count = 0;
        for(Movie m : movies){
            count++;
            m.setDirector(director);
            if(count>2){
                throw new RuntimeException("fake exception");
            }
            moviService.addMovie(m); // none
        }

        logger.info("registering director " + director);
        director.getMovies().forEach(m->logger.info("movie: " + m));
        return false;
    }

    @PostConstruct
    public void postConstruct(){
        logger.info("**** creating director registerer bean");
    }

    @PreDestroy
    public void preDestroy(){
        logger.info("**** destroying director registerer bean");
    }

}
