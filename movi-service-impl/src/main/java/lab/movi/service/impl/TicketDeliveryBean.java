package lab.movi.service.impl;

import lab.movi.model.Ticket;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Logger;

/*@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/TicketQueue")
        }
)*/
public class TicketDeliveryBean implements MessageListener {

    Logger logger = Logger.getLogger(TicketDeliveryBean.class.getName());

    @Override
    public void onMessage(Message message) {
        try {
            Ticket t = message.getBody(Ticket.class);
            logger.info("ticket delivery process started: " + t);

        } catch (JMSException e) {
            e.printStackTrace();
        }

    }
}
