package lab.movi.service;

import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;

import java.util.Set;

public interface MoviService {

    Set<Movie> getAllMovies();

    Set<Movie> getMoviesByDirector(Director d);

    Set<Movie> getMoviesInCinema(Cinema c);

    Movie getMovieById(int id);

    Set<Cinema> getAllCinemas();

    Set<Cinema> getCinemasByMovie(Movie m);

    Cinema getCinemaById(int id);

    Set<Director> getAllDirectors();

    Director getDirectorById(int id);

    Movie addMovie(Movie m);

    Director addDirector(Director d);
}
