package lab.movi.service;

import lab.movi.model.Director;
import lab.movi.model.Movie;

public interface DirectorRegisterer {

    void createDirector(Director director);

    void addMovie(Movie movie);

    boolean register();

}
