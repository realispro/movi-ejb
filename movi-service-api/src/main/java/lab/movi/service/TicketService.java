package lab.movi.service;

import lab.movi.model.Cinema;
import lab.movi.model.Movie;
import lab.movi.model.Ticket;

public interface TicketService {

    Ticket buyTicket(Cinema c, Movie m);

}
