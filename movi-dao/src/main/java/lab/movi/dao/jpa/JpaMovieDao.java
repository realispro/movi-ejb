package lab.movi.dao.jpa;

import lab.movi.dao.MovieDao;
import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Vetoed;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;


@Alternative
public class JpaMovieDao implements MovieDao {

    //@PersistenceContext(name = "moviUnit")
    private EntityManager em;


    @Override
    public Set<Movie> findAll() {
        return new HashSet<>(
                em.createQuery("from Movie").getResultList()
        );
    }

    @Override
    public Movie findById(int id) {
        return em.find(Movie.class, id);
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return new HashSet<>(
                em.
                        createQuery("select m from Movie m where m.director=:d").
                        setParameter("d", d).
                        getResultList()
        );
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return new HashSet<>(
                em.createQuery("select m from Movie m join m.cinemas c where c=:c")
                .setParameter("c", c)
                .getResultList()
        );
    }

    @Override
    public Movie add(Movie m) {
        em.persist(m);
        return m;
    }
}
