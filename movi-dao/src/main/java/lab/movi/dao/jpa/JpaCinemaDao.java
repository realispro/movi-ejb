package lab.movi.dao.jpa;

import lab.movi.dao.CinemaDao;
import lab.movi.model.Cinema;
import lab.movi.model.Movie;

import javax.enterprise.inject.Alternative;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Alternative
public class JpaCinemaDao implements CinemaDao {

    //@PersistenceContext(name = "moviUnit")
    private EntityManager em;

    @Override
    public Set<Cinema> findAll() {
        return new HashSet<>(
                em.createQuery("select c from Cinema c").getResultList()
        );
    }

    @Override
    public Cinema findById(int id) {
        return em.find(Cinema.class, id);
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return new HashSet<>(
                em.
                        createQuery("select c from Cinema c join c.movies m where m = :m").
                        setParameter("m", m).
                        getResultList()
        );
    }
}
