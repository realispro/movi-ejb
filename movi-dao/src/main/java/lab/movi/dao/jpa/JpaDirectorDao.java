package lab.movi.dao.jpa;

import lab.movi.dao.DirectorDao;
import lab.movi.model.Director;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.inject.Alternative;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Set;

@Alternative
public class JpaDirectorDao implements DirectorDao {

    //@PersistenceContext(name = "moviUnit")
    private EntityManager em;

    @Override
    public Set<Director> findAll() {
        return new HashSet<>(
                em.createQuery("from Director").getResultList()
        );
    }

    @Override
    public Director findById(int id) {
        return em.find(Director.class, id);
    }

    @Override
    public Director add(Director d) {
        em.persist(d);
        return d;
    }
}
