package lab.movi.dao.mem;

import lab.movi.dao.DirectorDao;
import lab.movi.model.Director;

import javax.enterprise.inject.Alternative;
import java.util.Set;

@Alternative
public class MemDirectorDao implements DirectorDao {
    @Override
    public Set<Director> findAll() {
        return SampleData.directors;
    }

    @Override
    public Director findById(int id) {
        return SampleData.directors.stream().filter(d->d.getId()==id).findFirst().orElse(null);
    }

    @Override
    public Director add(Director d) {
        int max = SampleData.directors.stream().max((d1,d2)->d1.getId()-d2.getId()).get().getId();
        d.setId(++max);
        SampleData.directors.add(d);
        return d;
    }
}
