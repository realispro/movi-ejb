package lab.movi.dao.mem;

import lab.movi.dao.CinemaDao;
import lab.movi.model.Cinema;
import lab.movi.model.Movie;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Vetoed;
import java.util.Set;
import java.util.stream.Collectors;

@Alternative
public class MemCinemaDao implements CinemaDao {
    @Override
    public Set<Cinema> findAll() {
        return SampleData.cinemas;
    }

    @Override
    public Cinema findById(int id) {
        return SampleData.cinemas.stream().filter(c->c.getId()==id).findFirst().orElse(null);
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return SampleData.cinemas.stream().filter(c->c.getMovies().contains(m)).collect(Collectors.toSet());
    }
}
