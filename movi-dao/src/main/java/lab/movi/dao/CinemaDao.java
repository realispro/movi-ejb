package lab.movi.dao;

import lab.movi.model.Cinema;
import lab.movi.model.Movie;

import java.util.Set;

public interface CinemaDao {

    Set<Cinema> findAll();

    Cinema findById(int id);

    Set<Cinema> findByMovie(Movie m);

}
