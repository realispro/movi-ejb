package lab.movi.dao.jdbc;

import lab.movi.dao.MovieDao;
import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;

import java.util.Set;

public class JdbcMovieDao implements MovieDao {
    @Override
    public Set<Movie> findAll() {
        return null;
    }

    @Override
    public Movie findById(int id) {
        return null;
    }

    @Override
    public Set<Movie> findByDirector(Director d) {
        return null;
    }

    @Override
    public Set<Movie> findByCinema(Cinema c) {
        return null;
    }

    @Override
    public Movie add(Movie m) {
        return null;
    }
}
