package lab.movi.dao.jdbc;

import lab.movi.dao.CinemaDao;
import lab.movi.model.Cinema;
import lab.movi.model.Movie;

import javax.sql.DataSource;
import java.util.Set;

public class JdbcCinemaDao implements CinemaDao {


    private DataSource dataSource;

    @Override
    public Set<Cinema> findAll() {
        return null;
    }

    @Override
    public Cinema findById(int id) {
        return null;
    }

    @Override
    public Set<Cinema> findByMovie(Movie m) {
        return null;
    }
}
