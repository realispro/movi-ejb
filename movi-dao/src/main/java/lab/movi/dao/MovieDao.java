package lab.movi.dao;

import lab.movi.model.Cinema;
import lab.movi.model.Director;
import lab.movi.model.Movie;

import java.util.Set;

public interface MovieDao {

    Set<Movie> findAll();

    Movie findById(int id);

    Set<Movie> findByDirector(Director d);

    Set<Movie> findByCinema(Cinema c);

    Movie add(Movie m);

}
