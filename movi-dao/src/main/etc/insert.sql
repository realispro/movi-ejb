create table cinema (
  id int primary key auto_increment,
  logo varchar(255) not null,
  name varchar(255) not null
);

create table director (
  id int primary key auto_increment,
  firstname varchar(255) not null,
  lastname varchar(255) not null
);

create table movie (
  id int primary key auto_increment,
  poster varchar(255) not null,
  rating float,
  title varchar(255) not null,
  director_id int
);

create table movie_cinema (
  movie_id int,
  cinema_id int
);

create table roles (
  id int primary key auto_increment,
  user_name varchar(255) not null,
  role_name varchar(255) not null
);

create table users (
  id int primary key auto_increment,
  user_name varchar(255) not null,
  password varchar(255) not null
);

INSERT INTO CINEMA(ID, LOGO, NAME) VALUES(1, 'https://www.kinoteka.pl/img/logo.png', 'Kinoteka');
INSERT INTO CINEMA(ID, LOGO, NAME) VALUES(2, 'http://www.festiwalfilmuniemego.pl/wp-content/uploads/2015/11/Kino-pod-Baranami.png', 'Kino pod baranami');
INSERT INTO CINEMA(ID, LOGO, NAME) VALUES(3, 'https://www.kinonh.pl/img/naglowek_pl2.png', 'Nowe Horyzonty');

INSERT INTO DIRECTOR(ID, FIRSTNAME, LASTNAME) VALUES(1, 'Steven', 'Spielberg');
INSERT INTO DIRECTOR(ID, FIRSTNAME, LASTNAME) VALUES(2, 'Woody', 'Allen');
INSERT INTO DIRECTOR(ID, FIRSTNAME, LASTNAME) VALUES(3, 'Guy', 'Ritchie');

INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(1, 'https://static.posters.cz/image/750webp/73584.webp', 2.2, 'Jaws', 1);
INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(2, 'https://fwcdn.pl/fpo/01/79/179/7710998.6.jpg', 8.1, 'Saving Private Ryan', 1);
INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(3, 'https://fwcdn.pl/fpo/12/15/1215/6918508.6.jpg', 7.1, 'E.T.', 1);

INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(4, 'https://upload.wikimedia.org/wikipedia/en/0/05/Vicky_Cristina_Barcelona_film_poster.png', 7.1, 'Vicky Cristina Barcelona', 2);
INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(5, 'https://upload.wikimedia.org/wikipedia/en/thumb/f/f3/Manhattan-poster01.jpg/220px-Manhattan-poster01.jpg', 7.1, 'Manhattan', 2);

INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(6, 'https://fwcdn.pl/fpo/13/26/1326/7635628.6.jpg', 7.1, 'Snatch', 3);
INSERT INTO MOVIE(ID, POSTER, RATING, TITLE, DIRECTOR_ID) VALUES(7, 'https://fwcdn.pl/fpo/19/97/441997/7239460.6.jpg', 7.1, 'RockNRolla', 3);


INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(1, 1);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(1, 3);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(2, 3);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(3, 1);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(3, 2);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(4, 1);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(4, 3);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(5, 2);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(5, 3);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(6, 1);
INSERT INTO MOVIE_CINEMA(MOVIE_ID, CINEMA_ID) VALUES(7, 2);