
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>


<header>
    <title>Moovi</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<c:url value = "/css/style.css"/>"/>
    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="./js/script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>

</header>

<body>
<header>
    <a href="./"><h1>Fake news</h1></a>
    <h2>Latest news</h2>
    <h3></h3>
</header>
<section>
    <table>
        <tr>
            <th>Cover</th>
            <th>Title</th>
        </tr>
        <c:forEach var="n" items="${news}">
            <tr>
                <td>
                    <img src="${n.cover}"/>
                </td>
                <td>
                    <a href="${n.link}">${n.title}</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</section>
</body>