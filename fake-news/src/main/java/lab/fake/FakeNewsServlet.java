package lab.fake;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

public class FakeNewsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("news", Arrays.asList(
                new FakeNews(
                        "5G causes coronavirus",
                        "https://eu.usatoday.com/story/tech/columnist/2020/04/20/dispelling-belief-5-g-networks-spreading-coronavirus/5148961002/",
                        "https://telecoms.com/wp-content/blogs.dir/1/files/2020/04/coronavirus-5g-conspiracy-770x285.jpg"),
                new FakeNews(
                        "Domestos kills coronavirus",
                        "https://edition.cnn.com/2020/04/24/politics/donald-trump-coronavirus-disinfectant-sunlight-science/index.html",
                        "https://cdn.cnn.com/cnnnext/dam/assets/200423204526-trump-exlarge-169.jpg"),
                new FakeNews(
                        "Elvis still alive",
                        "https://www.dailymail.co.uk/tvshowbiz/article-8241009/Freddie-Flintoff-claims-Elvis-Presley-ALIVE.html",
                        "https://i.dailymail.co.uk/1s/2020/04/21/14/27463690-8241009-image-a-98_1587477008570.jpg"),
                new FakeNews(
                        "Earth is flat!",
                        "https://www.tfes.org",
                        "https://d.newsweek.com/en/full/1421915/gettyimages-1129638660.webp?w=737&f=c44b8162911aa501c0de4428f8e2e4bd")
        ));
        req.getRequestDispatcher("/index.jsp").forward(req,resp);
    }
}
