package lab.fake;

public class FakeNews {

    private String title;

    private String link;

    private String cover;

    public FakeNews(String title, String link, String cover) {
        this.title = title;
        this.link = link;
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getCover() {
        return cover;
    }
}
