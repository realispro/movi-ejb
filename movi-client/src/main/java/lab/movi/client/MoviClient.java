package lab.movi.client;

import lab.movi.model.Movie;
import lab.movi.service.MoviServiceRemote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import java.util.Set;

public class MoviClient {
    public static void main(String[] args) {
        System.out.println("Movi Client");

        try {

            // config
            String moviServiceName =
                    //"java:jboss/exported/" +
                            "movi-ear-1.0/movi-service-impl-1.0/MoviService!lab.movi.service.MoviServiceRemote";
           /* Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
            props.put(Context.PROVIDER_URL, "http-remoting://localhost:8080"); // JBoss EAP 7
            //props.put(Context.PROVIDER_URL, "remote://localhost:4447"); // JBoss EAP 6
            props.put("jboss.naming.client.ejb.context", true);*/

            // instance obtaining
            Context context = new InitialContext();
            MoviServiceRemote moviService = (MoviServiceRemote)context.lookup(moviServiceName);

            Set<Movie> movies = moviService.getAllMovies();
            movies.forEach(System.out::println);

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
}
