package lab.movi.client;

import lab.movi.model.Director;
import lab.movi.model.Movie;
import lab.movi.service.DirectorRegisterer;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DirectorRegistererClient {

    public static void main(String[] args) {
        System.out.println("DirectorRegistererClient.main");


        String beanName = "movi-ear-1.0/movi-service-impl-1.0/DirectorRegistererBean!lab.movi.service.DirectorRegisterer";

        try {
            Context context = new InitialContext();
            DirectorRegisterer registerer = (DirectorRegisterer) context.lookup(beanName);

            Director polanski = new Director();
            polanski.setFirstName("Roman");
            polanski.setLastName("Polanski");

            registerer.createDirector(polanski);

            Movie rosemary = new Movie();
            rosemary.setTitle("Dziecko Rosemary");
            rosemary.setRating(3.8F);
            rosemary.setPoster("https://fwcdn.pl/fpo/11/04/1104/7879809.6.jpg");
            registerer.addMovie(rosemary);

            Movie noz = new Movie();
            noz.setTitle("N\u00F3\u017c w wodzie");
            noz.setRating(3.9f);
            noz.setPoster("https://fwcdn.pl/fpo/08/94/894/7879825.3.jpg");
            registerer.addMovie(noz);

            Movie pianista = new Movie();
            pianista.setTitle("Pianista");
            pianista.setRating(4.2f);
            pianista.setPoster("https://fwcdn.pl/fpo/22/25/32225/7519150.3.jpg");
            registerer.addMovie(pianista);

            Movie wrota = new Movie();
            wrota.setTitle("Dziewiate Wrota");
            wrota.setPoster("https://1.bp.blogspot.com/-E_LBI9m4szI/VP4NBRy6b6I/AAAAAAAAaW4/TFD-DOVORac/s1600/1.jpg");
            wrota.setRating(3.7f);
            registerer.addMovie(wrota);
            
            boolean success = registerer.register();
            System.out.println("success = " + success);

            registerer = null;

            System.gc();
            Thread.sleep(10000);

            System.out.println("done.");


        } catch (NamingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
